# 1st Level
## Send confirmation e-mail
- Ticket was created
## Check if emergency
- Production server down
- Users can't login (Production)
- Users can't print Reports (Production)
- No workaround for time limited usertask (Production)
- Chef want to show something ; )
## Check if Fleetmanager or PowerUser
z.B. LU  
**Fleetmanager:** Yvonne  
**PowerUser:** Paul, René

## Can handle it (immediately)
- handle it
- close EgroupTicket
- change template state to resolved
- send confirmation to customer (Template DE/FR)

## Can handle it (planning required)
- discuss with project manager
    - solution
    - dueDate
    - User impact
    - work todo
    - time needed
    - needed employees
- make appointment with Fleetmanager
- schedule in Outlook Calendar (send invitations)
			

## Can't handle it
- collect all informations
- note intuitions
- forward template to 2. Level (Application responsible)